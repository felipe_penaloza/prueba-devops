FROM node:8 as build

COPY package*.json ./
RUN npm install
FROM node:8-slim
RUN npm install koa
RUN npm install koa-router
COPY . .
EXPOSE 3000
CMD [ "node", "index.js" ]
